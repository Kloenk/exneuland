let
  sources = import ./nix/sources.nix {};
in

with import sources.nixpkgs {};

let
  packages = beam.packagesWith beam.interpreters.erlang;
  src = lib.cleanSource ./.;
  pname = "exneuland";
  version = "0.0.1";
  mixEnv = "prod";

  mixDeps = packages.fetchMixDeps {
    pname = "mix-deps-${pname}";
    inherit src mixEnv version;
    sha256 = "0598sgxrjcna0sihmg21sjzdn5mbfbvq63ccwgry9ikhkr863yhh";
  };

  pkg = (packages.mixRelease.override {
    elixir = (import sources.nixpkgs-unstable {}).elixir_1_12;
  }) {
    inherit src pname version mixEnv mixDeps;

    buildInputs = [ rebar3 protobuf ];
  };

  dockerScript = writeScript "start_exneuland" ''
    #!/${busybox}/bin/sh
    ${pkg}/erts-*/bin/epmd -daemon
    ${pkg}/bin/exneuland start
  '';

  dockerConf = {
    name = "exneuland"; 
    tag = "latest"; 
    contents = [ gnused coreutils gnugrep ];
    created = "now";
    config.Cmd = [ "${dockerScript}" ];
    config.ExposedPorts = {
      "4000" = {}; # exneuland
      "4369" = {}; # EPMD
    };
  };

  dockerImage = dockerTools.buildLayeredImage dockerConf;
  dockerStream = dockerTools.streamLayeredImage dockerConf;

in pkg // { inherit dockerImage dockerStream; }
