defmodule Exneuland.Hub do
  use HTTPoison.Base

  defp hub_url, do: Application.fetch_env!(:exneuland, :hub_url)
  defp hub_token, do: Application.fetch_env!(:exneuland, :hub_token)

  @expected_fields ~w(
    nickname color organizationSlug worldSlug roomSlug
    userUuid oldUuid messages textures characterLayers
  )

  def process_request_url(url) do
    "#{hub_url}" <> url
  end

  def process_request_headers(headers) do
    [
      {"Authorization", hub_token}
      | Enum.filter(headers, fn {header, _value} -> String.downcase(header) != "authorization" end)
    ]
  end

  def process_response_body(body) do
    case Poison.decode(body) do
      {:ok, value} ->
        value
        |> Map.take(@expected_fields)
        |> Enum.map(fn {k, v} -> {String.to_atom(k), v} end)

      _ ->
        body
    end
  end
end
