defmodule Exneuland.Rooms do
  @doc """
    Send `msg` to all players in `room`.

    This notifies the process associated with each player; it will then send
    appropriate websocket messages in `Exneuland.SocketHandler.websocket_info`.
  end
  """
  def broadcast(msg, room), do: dispatch(Exneuland.Rooms, room, msg)

  @doc """
    Send `msg` to a specific player defined by `key`.
  """
  def send_to(msg, {_room, _uid} = key), do: dispatch(Exneuland.Players, key, msg)

  @doc """
    Send `msg` to a specific group's process matching the given `key`.
  """
  def cast_group(msg, {_room, _gid} = key) do
    with [{pid, _}] <- Registry.lookup(Exneuland.Group, key) do
      GenServer.cast(pid, msg)
    end
  end

  # helper function; sends `msg` to all processes in `registry`
  # that match the `key` but are not the sending process.
  defp dispatch(registry, key, msg) do
    Registry.dispatch(registry, key, fn entries ->
      for {pid, _} <- entries do
        if pid != self() do
          send(pid, msg)
        end
      end
    end)
  end
end
