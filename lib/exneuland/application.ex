defmodule Exneuland.Application do
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      {Cluster.Supervisor,
       [Application.get_env(:libcluster, :topologies), [name: Exneuland.ClusterSupervisor]]},
      {
        Plug.Cowboy,
        scheme: :http,
        plug: Exneuland.Router,
        options: [
          port: Application.fetch_env!(:exneuland, :port),
          dispatch: dispatch()
        ]
      },
      # rooms (i.e. separate maps); each key maps to all clients currently
      # in that room
      {Registry, keys: :duplicate, name: Exneuland.Rooms},
      # room shards, services that keep track and centrally steer messages
      # for each room on an individual instance
      {Registry, keys: :unique, name: Exneuland.Roomshards},
      # groups (i.e. voicechat circles); each key maps to a single process
      # handling that group
      {Registry, keys: :unique, name: Exneuland.Group},
      # players; each key maps to a single process handling websocket messages
      # from and to that client
      {Registry, keys: :unique, name: Exneuland.Players},
      %{
        id: Exneuland.Cluster.NodeWatcher,
        start: {Exneuland.Cluster.NodeWatcher, :start_link, []}
      }
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Exneuland.Supervisor]
    Supervisor.start_link(children, opts)
  end

  defp dispatch do
    [
      {
        :_,
        [
          {"/room", Exneuland.SocketHandler, []},
          {:_, Plug.Cowboy.Handler, {Exneuland.Router, []}}
        ]
      }
    ]
  end
end
