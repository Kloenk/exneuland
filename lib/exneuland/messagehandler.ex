defmodule Exneuland.MessageHandler do
  alias Workadventure.Messages

  @moduledoc """
  High-level message handler; this module contains the logic to deal with
  messages from clients; parsing, sending, etc. are abstracted away by
  `Exneuland.SocketHandler`.
  """

  @doc """
  handles a decoded websocket message from a client, returning an updated
  version of `state`; see comments for how different message types are handled.

  Will log the message in case we don't yet know it / haven't implemented its
  type yet.
  """
  # the player switched silent mode on or off
  def handle({:silentMessage, message}, state) do
    {:ok, %{state | silent: message.silent}}
  end

  # the user moved; send new position to others
  def handle({:userMovesMessage, msg}, state) do
    update = {
      :userMovedMessage,
      %Messages.UserMovedMessage{
        userId: state.uid,
        position: msg.position
      }
    }

    Exneuland.Rooms.broadcast(update, state.room)

    {:ok, %{state | position: msg.position, viewport: msg.viewport}}
  end

  # this user's viewport changed (might be useful for e.g. filtering updates
  # from clients outside that viewport, though this is not currently done)
  def handle({:viewportMessage, msg}, state) do
    {:ok, %{state | viewport: msg}}
  end

  # apparently nothing happened?
  def handle(nil, state), do: {:ok, state}

  # relay ICE messages between clients (clients use these to negotiate
  # peer-to-peer webRTC connections for audio/video chats)
  def handle({:webRtcSignalToServerMessage, msg}, state) do
    if msg.receiverId in state.peers do
      inner = %Messages.WebRtcSignalToClientMessage{
        userId: state.uid,
        signal: msg.signal
      }

      {:proto, %Messages.ServerToClientMessage{message: {:webRtcSignalToClientMessage, inner}}}
      |> Exneuland.Rooms.send_to({state.room, msg.receiverId})
    else
      IO.puts("Stray WebRTC Signal from #{state.name}@#{state.uid}")
    end

    {:ok, state}
  end

  def handle({:emotePromptMessage, msg}, state) do
    Exneuland.Rooms.broadcast(
      {:emoteEventMessage, %Messages.EmoteEventMessage{actorUserId: state.uid, emote: msg.emote}},
      state.room
    )

    {:ok, state}
  end

  # a fallthrough for all other kinds of messages (which we haven't implemented
  # yet / might not need at all); just logs the message.
  def handle(message, state) do
    IO.inspect(message)
    {:ok, state}
  end
end
