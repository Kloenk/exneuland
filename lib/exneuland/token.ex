defmodule Exneuland.Token do
  use Joken.Config

  # token valid for a week, audience workadventure, issuer exneuland
  def token_config,
    do: default_claims(default_exp: 60 * 60 * 24 * 7, aud: "WorkAdventure", iss: "ExNeuland")

  def get_signer,
    do: Joken.Signer.create("HS256", Application.fetch_env!(:exneuland, :token_secret))
end
