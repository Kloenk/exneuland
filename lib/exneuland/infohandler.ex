defmodule Exneuland.InfoHandler do
  alias Workadventure.Messages

  @moduledoc """
    Handles "info" messages for the SocketHandler, i.e. messages that are not
    websocket messages sent from the client, but messages sent within the VM
    from another process.
  """

  @doc """
  translates a higher-level message into the corresponding protobuf message,
  using information from `state` if needed.
  """
  # new client joined; leaves `state` unchanged
  def handle({:hello, msg}, state) do
    reply = {:userJoinedMessage, Messages.user_joined_from_state(state)}
    Exneuland.Rooms.send_to(reply, {state.room, msg.userId})

    Exneuland.SocketHandler.queue({:userJoinedMessage, msg}, state)
  end

  # some other client moved; if it is close enough, start a group
  # with it; leaves the state unchanged.
  def handle({:userMovedMessage, msg} = cmsg, state) do
    dist = Messages.distance(msg.position, state.position)
    moving = msg.position.moving or state.position.moving

    cond do
      dist <= 60 and !state.silent and !moving and state.group == nil ->
        {:initGroup, {state.uid, state.name, state.position}}
        |> Exneuland.Rooms.send_to({state.room, msg.userId})

      true ->
        nil
    end

    Exneuland.SocketHandler.queue(cmsg, state)
  end

  # some other client wants to talk to ours; register a group with a random ID,
  # and add it to our state.
  def handle({:initGroup, peer}, state) do
    if state.group == nil and !state.silent do
      gid = Enum.random(0..2_147_483_647)
      Exneuland.Group.start(state.room, gid, [{state.uid, state.name, state.position}, peer])
      {:ok, %{state | group: gid}}
    else
      {:ok, state}
    end
  end

  # a group noticed us and would like us to join it.
  def handle({:joinGroup, gid}, state) do
    if state.group == nil and !state.silent and !state.position.moving do
      Exneuland.Rooms.cast_group(
        {:join, {state.uid, state.name, state.position}},
        {state.room, gid}
      )

      {:ok, %{state | group: gid}}
    else
      {:ok, state}
    end
  end

  # webRTC connection started with another client; add it to our list of peers
  def handle({:webRtcStartMessage, msg} = cmsg, state) do
    Exneuland.SocketHandler.send_proto(
      %Messages.ServerToClientMessage{message: cmsg},
      %{state | peers: [msg.userId | state.peers]}
    )
  end

  # webRTC connection with another client closed; remove it from our list of peers
  def handle({:webRtcDisconnectMessage, msg} = cmsg, state) do
    group =
      case length(Enum.uniq(state.peers)) do
        1 -> nil
        _ -> state.group
      end

    Exneuland.SocketHandler.send_proto(
      %Messages.ServerToClientMessage{message: cmsg},
      %{state | peers: Enum.reject(state.peers, &(&1 == msg.userId)), group: group}
    )
  end

  # queue things that look like they ought to be queued and somehow didn't end
  # up in Exneuland.SocketHandler.websocket_info (?)
  #
  # TODO: this explanation is almost certainly wrong, since websocket_info would
  # strip the queue-mode atom before invoking Sockethandler.queue
  def handle({t, msg}, state) do
    Exneuland.SocketHandler.queue({t, msg}, state)
  end

  # log unknown messages.
  def handle(msg, state) do
    IO.puts("unknown info: #{inspect(msg)}")
    {:ok, state}
  end
end
