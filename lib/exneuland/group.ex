defmodule Exneuland.Group do
  alias Workadventure.Messages
  use GenServer

  @moduledoc """
    Contains functions that control groups, i.e. the audio/video chat bubbles.
  """

  @doc """
  Spawn a process representing a new group.
  """
  def start(room, gid, participants) do
    # IO.puts "Group #{gid} in #{room}: #{inspect participants}"
    GenServer.start(__MODULE__, {room, gid, participants})
  end

  @doc """
  Called for each new group; `participants` is a list of exactly two clients.any()

  Registers this group in appropriate registries, then calls `loop/1`
  """
  @impl true
  def init({room, gid, participants}) do
    [{uA, nA, pA}, {uB, _nB, _pB}] = participants

    state = %{
      gid: gid,
      room: room,
      center: %{x: 0, y: 0},
      participants: Map.new([{uA, {nA, pA}}])
    }

    Registry.register(Exneuland.Group, {room, gid}, {})
    Registry.register(Exneuland.Rooms, room, {})

    request_join(gid, room, uB)
    # Recalculate memberships after a second, in case init didn't work
    Process.send_after(self(), :calc, 1000)

    {:ok, state}
  end

  @impl true
  def handle_cast({:join, {uid, name, position}}, state) do
    for {peer, {_pname, _ppos}} <- state.participants do
      connect(peer, uid, state.room)
      connect(uid, peer, state.room)
    end

    calc(%{state | participants: Map.put(state.participants, uid, {name, position})})
  end

  def handle_cast(:terminate, state) do
    {:stop, {:shutdown, :empty}, state}
  end

  @impl true
  # Some user moved. Remove it from our list of participants if it was one before
  # and walked too far away; ask it to join this group if it wasn't a participant
  # before and now got close enough
  def handle_info({:userMovedMessage, msg}, state) do
    dist = Messages.distance(msg.position, state.center)

    if msg.userId in Map.keys(state.participants) do
      if dist > 60 do
        leave(msg.userId, state)
      else
        newpart =
          Map.update!(state.participants, msg.userId, fn {name, _pos} -> {name, msg.position} end)

        calc(%{state | participants: newpart})
      end
    else
      if dist < 45 and length(Map.keys(state.participants)) < 4 do
        request_join(state.gid, state.room, msg.userId)
      end

      {:noreply, state}
    end
  end

  # TODO: don't understand this one; it doesn't seem like anyone sends :hello to
  # a group, ever?
  def handle_info({:hello, msg}, state) do
    reply = {
      :groupUpdateMessage,
      %Messages.GroupUpdateMessage{
        groupId: state.gid,
        position: %Messages.PointMessage{x: state.center.x, y: state.center.y},
        groupSize: 60
      }
    }

    Exneuland.Rooms.send_to(reply, {state.room, msg.userId})
    {:noreply, state}
  end

  # one of our participants left
  def handle_info({:userLeftMessage, msg}, state), do: leave(msg.userId, state)
  # recalculate the group's circle (sent by init/3 with a delay to make sure
  # things work as expected)
  def handle_info(:calc, state), do: calc(state)
  # ignore other message
  def handle_info(_, state), do: {:noreply, state}

  # recalculates the centre of this group (e.g. because someone moved)
  defp calc(state) do
    if length(Map.keys(state.participants)) > 1 do
      newcenter = recenter(state)
      {:noreply, %{state | center: newcenter}}
    else
      {:stop, {:shutdown, :empty}, state}
    end
  end

  # remove a participant from our state
  defp leave(uid, state) do
    newpart = Map.delete(state.participants, uid)

    for {peer, _data} <- newpart do
      disconnect(peer, uid, state.room)
      disconnect(uid, peer, state.room)
    end

    calc(%{state | participants: newpart})
  end

  @impl true
  # stop this group's process
  def terminate(_reason, state) do
    Exneuland.Rooms.broadcast(
      {:groupDeleteMessage, %Messages.GroupDeleteMessage{groupId: state.gid}},
      state.room
    )
  end

  defp connect(target, source, room) do
    Exneuland.Rooms.send_to(
      {:webRtcStartMessage,
       %Messages.WebRtcStartMessage{userId: target, initiator: target > source}},
      {room, source}
    )
  end

  defp disconnect(p1, p2, room) do
    Exneuland.Rooms.send_to(
      {:webRtcDisconnectMessage, %Messages.WebRtcDisconnectMessage{userId: p1}},
      {room, p2}
    )
  end

  def request_join(gid, uid, room) do
    Exneuland.Rooms.send_to({:joinGroup, gid}, {uid, room})
  end

  defp recenter(state) do
    {x, y} = Messages.middle(Enum.map(state.participants, fn {_uid, {_name, pos}} -> pos end))

    Exneuland.Rooms.broadcast(
      {
        :groupUpdateMessage,
        %Messages.GroupUpdateMessage{
          groupId: state.gid,
          position: %Messages.PointMessage{x: x, y: y},
          groupSize: 60
        }
      },
      state.room
    )

    %{x: x, y: y}
  end
end
