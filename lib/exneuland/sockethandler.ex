defmodule Exneuland.SocketHandler do
  @moduledoc """
  The functions in this module implement the :cowboy_websocket behaviour, which
  handles a single websocket connection in one process of the VM. Each of these
  processes will be registered in appropriate registries so that others can find
  it (`Exneuland.Groups`, `Exneuland.Players`, and `Exneuland.Rooms`).

  Note that most of the actual logic is done in `Exneuland.MessageHandler`; this
  module handles only the initial setup as well as message parsing and (queued)
  message sending.

  For documentation on the :cowboy_websocket behaviour, refer to
  [ninenines.eu](https://ninenines.eu/docs/en/cowboy/2.7/manual/cowboy_websocket/).
  """
  @behaviour :cowboy_websocket
  alias Workadventure.Messages

  defp parse_int(x) when is_bitstring(x), do: Integer.parse(x)
  defp parse_int(_), do: :error

  @doc """
  Builds an initial state from the client's first request,
  which is then passed to `websocket_init/1`
  """
  def init(request, _state) do
    args =
      request.qs
      |> String.replace("characterLayers", "characterLayers[]")
      |> String.replace("characterLayer=", "characterLayers[]=")
      |> Plug.Conn.Query.decode()
      |> Enum.map(fn {k, v} ->
        case parse_int(v) do
          :error -> {k, v}
          {x, _} -> {k, x}
        end
      end)
      |> Map.new()

    with {:ok, claims} <-
           Exneuland.Token.verify_and_validate(args["token"], Exneuland.Token.get_signer()) do
      viewport = %Messages.ViewportMessage{
        left: args["left"],
        top: args["top"],
        right: args["right"],
        bottom: args["bottom"]
      }

      position = %Messages.PositionMessage{
        x: args["x"],
        y: args["y"],
        direction: :DOWN,
        moving: false
      }

      charLayers =
        args["characterLayers"]
        |> Enum.map(&%Messages.CharacterLayerMessage{name: &1})

      state = %{
        room: args["roomId"],
        name: args["name"],
        token: args["token"],
        character: charLayers,
        companion: %Messages.CompanionMessage{name: Map.get(args, "companion", "")},
        position: position,
        viewport: viewport,
        silent: false,
        uid: Enum.random(0..2_147_483_647),
        uuid: claims["userUuid"],
        queue: [],
        peers: [],
        group: nil
      }

      {:cowboy_websocket, request, state}
    else
      _ -> {:cowboy_websocket, request, :invalid}
    end
  end

  def websocket_init(:invalid) do
    reply = %Messages.ServerToClientMessage{
      message: {:tokenExpiredMessage, %Messages.TokenExpiredMessage{}}
    }

    # reply = %Messages.ServerToClientMessage{message: {:worldConnexionMessage, %Messages.WorldConnexionMessage{message: "no access"}}}
    with {:ok, binmsg} <- Protox.Encode.encode(reply) do
      {[{:binary, binmsg}, :close], :invalid}
    else
      _ -> {[:close], :invalid}
    end
  end

  @doc """
  At this point, the connection was updated to be a websocket.
  Registers this process in the appropriate registries,
  broadcasts an announcement about the new client to others,
  and sends a response to this one.
  """
  def websocket_init(state) do
    Registry.register(Exneuland.Rooms, state.room, {})
    Registry.register(Exneuland.Players, {state.room, state.uid}, {})

    reply =
      %Messages.RoomJoinedMessage{currentUserId: state.uid}
      |> (fn m -> %Messages.ServerToClientMessage{message: {:roomJoinedMessage, m}} end).()

    Exneuland.Rooms.broadcast({:hello, Messages.user_joined_from_state(state)}, state.room)
    send_proto(reply, state)
  end

  def terminate(_reason, _preq, :invalid), do: :invalid

  @doc """
  The connection to the client was closed; broadcast a leave message to others.
  """
  def terminate(_reason, _preq, state) do
    leave = {:userLeftMessage, %Messages.UserLeftMessage{userId: state.uid}}
    Exneuland.Rooms.broadcast(leave, state.room)
  end

  @doc """
  Incoming binary messages are decoded and handled via
  `Exneuland.MessageHandler.handle/2`; messages that can't be decoded via
  protobuf or are not binary are logged and otherwise ignored.
  """
  def websocket_handle({:binary, message}, state) do
    with {:ok, msg} <- Messages.ClientToServerMessage.decode(message) do
      Exneuland.MessageHandler.handle(msg.message, state)
    else
      err ->
        IO.puts("couldn't decode incoming msg: #{inspect(err)}")
        {:ok, state}
    end
  end

  def websocket_handle({:ping, _}, state), do: {:ok, state}

  def websocket_handle(msg, state) do
    IO.puts("unexpected message! #{inspect(msg)}")
    {:ok, state}
  end

  @doc """
  called for messages sent from other processes in the VM.

  Has two modes: if `msg` is a tuple `{mode, msg}` where `mode` is one of
  `:send` `:proto` `:queue` or `flush`, the message will be forwarded via
  websocket directly to the client (using a queue controlled by `mode`).any()

  Otherwise it is passed to `Exneuland.InfoHandler.handle/2`, which translates
  the higher-level messages sent by other processes into a lower-level protobuf
  message, and then send that.
  """
  def websocket_info({:send, msg}, state), do: {:reply, msg, state}

  def websocket_info({:proto, msg}, state), do: send_proto(msg, state)

  def websocket_info({:queue, msg}, state), do: queue(msg, state)

  def websocket_info(:flush, state), do: flush(state)

  def websocket_info(msg, state) do
    Exneuland.InfoHandler.handle(msg, state)
  end

  @doc """
  encodes a protobuf message and sends it over the websocket.
  """
  def send_proto(msg, state) do
    with {:ok, binmsg} <- Protox.Encode.encode(msg) do
      {:reply, {:binary, binmsg}, state}
    else
      err ->
        IO.puts("couldn't encode outgoing msg: #{inspect(err)}, #{inspect(msg)}")
        {:ok, state}
    end
  end

  @doc """
  adds either a single message or a list of messages to the message queue
  attribute of the `state` tuple (used to send messages to the client in bulk).
  """
  def queue(msgs, state) when is_list(msgs) do
    if state.queue == [] do
      Process.send_after(self(), :flush, 200)
    end

    {:ok, %{state | queue: msgs ++ state.queue}}
  end

  def queue(msg, state) do
    queue([msg], state)
  end

  # sends all messages that are in the queue to the client
  defp flush(state) do
    batchPayload =
      state.queue
      |> Enum.map(&%Messages.SubMessage{message: &1})
      |> Enum.reverse()

    batchMsg = %Messages.BatchMessage{payload: batchPayload}
    msg = %Messages.ServerToClientMessage{message: {:batchMessage, batchMsg}}
    websocket_info({:proto, msg}, %{state | queue: []})
  end
end
