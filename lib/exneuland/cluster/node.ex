defmodule Exneuland.Cluster.NodeWatcher do
  use GenServer

  def start_link do
    GenServer.start_link(__MODULE__, nil, name: __MODULE__)
  end

  def request_copy(node) do
    GenServer.call({__MODULE__, node}, :copy)
  end

  def query_registry(registry, key), do: query_registry(Node.self(), registry, key)

  def query_registry(node, registry, key) do
    GenServer.call({__MODULE__, node}, {:registry_query, registry, key})
  end

  def query_all(registry, key) do
    Node.list()
    |> Enum.map(&query_registry(&1, registry, key))
    |> List.foldl([], &++/2)
  end

  # side note, migrating tables
  # tldr; scale-down to one instance, then :mnesia.transform_table/3
  # see https://elixirschool.com/en/lessons/storage/mnesia for competent info
  @tables [
    Exneuland.Store.User,
    Exneuland.Store.Map
  ]

  @impl true
  def init(_) do
    Process.sleep(1000)
    :net_kernel.monitor_nodes(true)
    Memento.add_nodes(Node.list())

    # uncomment in production to allow for slow starts
    # TODO: deal with health and readiness checks somehow to get this working cleanly,
    # right now we can splitbrain easily initially
    # with [] <- Node.list do
    #   IO.puts "no nodes yet, waiting..."
    #   Process.sleep(30000)
    # end

    # this can potentially cause split-brain and break stuff badly, requiring a full cluster restart.
    # let's hope it never happens
    case Node.list() do
      [] ->
        IO.puts("no other nodes, creating tables")
        Enum.each(@tables, &Memento.Table.create!/1)

      _ ->
        node = Enum.random(Node.list())
        IO.puts("asking #{Atom.to_string(node)} for tables")
        request_copy(node)
    end

    {:ok, %{}}
  end

  @impl true
  # request for tables to be copied
  def handle_call(:copy, {pid, _tag}, state) do
    Enum.each(@tables, &Memento.Table.create_copy(&1, node(pid), :ram_copies))
    {:reply, :ok, state}
  end

  # query the local registry and pass the processes back
  def handle_call({:registry_query, registry, key}, state) do
    {:reply, Registry.lookup(registry, key), state}
  end

  @impl true
  # Allow remote dispatching from other hosts
  def handle_cast({:dispatch, registry, key, message}, state) do
    Registry.dispatch(
      registry,
      key,
      fn entries ->
        for x <- entries do
          send(x, message)
        end
      end
    )

    {:noreply, state}
  end

  @impl true
  def handle_info({:nodeup, node}, state) do
    Memento.add_nodes(node)
    {:noreply, state}
  end

  def handle_info({:nodedown, _node}, state) do
    {:noreply, state}
  end

  def handle_info(_, state), do: {:noreply, state}

  @impl true
  def terminate(_reason, _state) do
    false
  end
end
