# A shard of a room. Contains all the clients that are connected to
# the current host in a specific room.
defmodule Exneuland.Cluster.Roomshard do
  use GenServer

  @impl true
  def init(room) do
    # find existing shards
    others = Exneuland.Cluster.NodeWatcher.query_all(Exneuland.Roomshards, room)
    Registry.register(Exneuland.Roomshards, room, {})

    # register with existing shards
    for pid <- others do
      GenServer.cast(pid, {:newshard, self()})
    end

    {:ok, %{room: room, others: others}}
  end

  @impl true
  # register a new shard
  def handle_cast({:newshard, pid}, %{others: others} = state) do
    {:noreply, %{state | others: [pid | others]}}
  end

  # broadcast a message to all local sockets
  def handle_cast({:broadcast, message}, state) do
    Exneuland.Rooms.broadcast(message, state.room)
    {:noreply, state}
  end

  # cast a message to all participants in a room across all hosts
  defp broadcast(message, state) do
    for other <- [self() | state.others] do
      GenServer.cast(other, {:broadcast, message})
    end

    {:noreply, state}
  end
end
