defmodule Exneuland.Store.Map do
  use Memento.Table,
    attributes: [:key, :url, :badges]
end
