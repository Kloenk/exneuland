defmodule Exneuland.Store.User do
  use Memento.Table,
    attributes: [:uuid, :nickname, :color, :messages, :textures, :layers, :active]
end
