defmodule Exneuland.Router do
  alias Exneuland.Store
  use Plug.Router

  if Mix.env() == :dev do
    use Plug.Debugger, otp_app: :exneuland
  end

  use Plug.ErrorHandler

  @moduledoc """
  Defines http routes we can serve. Currently defaults to 404 for everything;
  the actually useful routes are defined directly in `Exneuland.Application`.
  """

  plug(:match)

  plug(Plug.Parsers,
    parsers: [:urlencoded, :json],
    json_decoder: Poison
  )

  plug(:dispatch)

  @event_slug Application.fetch_env!(:exneuland, :event_slug)

  get "/verify" do
    args = Plug.Conn.Query.decode(conn.query_string)
    token = Map.get(args, "token", "")

    case Exneuland.Token.verify_and_validate(token, Exneuland.Token.get_signer()) do
      {:ok, _jwt} -> send_resp(conn, 200, Poison.encode!(%{success: true}))
      _ -> send_resp(conn, 401, Poison.encode!(%{error: "Invalid JWT"}))
    end
  end

  post "/register" do
    registerToken = Map.get(conn.params, "organizationMemberToken", "")

    resp =
      case Exneuland.Hub.get("/api/c/#{@event_slug}/workadventure/login/#{registerToken}") do
        {:ok, %HTTPoison.Response{status_code: 200, body: body}} -> body
        _ -> raise("Hub Login Failed")
      end

    room = "/@/#{resp[:organizationSlug]}/#{resp[:worldSlug]}/#{resp[:roomSlug]}"

    newUser = %Store.User{
      uuid: resp[:userUuid],
      nickname: resp[:nickname],
      color: resp[:color],
      messages: resp[:messages],
      textures: resp[:textures],
      layers: resp[:characterLayers],
      active: true
    }

    Memento.transaction(fn -> Memento.Query.write(newUser) end)

    {:ok, jwt, _tokenPayload} =
      Exneuland.Token.generate_and_sign(%{userUuid: newUser.uuid}, Exneuland.Token.get_signer())

    send_resp(
      conn,
      200,
      Poison.encode!(
        %{userUuid: newUser.uuid, roomUrl: room, textures: newUser.textures, authToken: jwt}
      )
    )
  end

  get "/map" do
    playUri = Map.get(conn.params, "playUri", "")

    {type, rem} =
      case Regex.run(~r|https?://[a-zA-Z0-9\.]+(?:\:\d{1,5})?/(.)/(.+)|, playUri) do
        [^playUri, type, rem] -> {type, rem}
        _ -> raise("Invalid map URL")
      end

    key =
      case type do
        "@" ->
          case Regex.run(~r|([^/]+)/([^/]+)/(.+)|, rem) do
            [^rem, org, assembly, url] -> "#{org}/#{assembly}/#{url}"
            _ -> raise("invalid map format")
          end

        "_" ->
          raise("No anonymous maps allowed")
      end

    with {:ok, result} <-
           Memento.transaction(fn -> Memento.Query.read(Store.Map, key) end) do
      IO.inspect(result)

      response = %{
        roomSlug: key,
        mapUrl: result.url,
        policyType: 0,
        tags: [],
        textures: []
      }

      send_resp(
        conn,
        200,
        Poison.encode!(response)
      )
    else
      _ -> send_resp(conn, 404, Poison.encode!(%{error: "Map Not Found"}))
    end
  end

  post "/anonymLogin" do
    # payload = %{userUuid: UUID.uuid4()}

    # {:ok, jwt, _tokenPayload} =
    # Exneuland.Token.generate_and_sign(payload, Exneuland.Token.get_signer())

    # send_resp(conn, 200, Poison.Encoder.encode(Map.put(payload, :authToken, jwt), %{}))
    send_resp(conn, 401, Poison.encode!(%{error: "Anonymous login is disabled."}))
  end

  get "/metrics" do
    players = Registry.select(Exneuland.Rooms, [{{:"$1", :_, :_}, [], [:"$1"]}])
    playercount = length(players)

    metrics_string = """
    # HELP exneuland_clients Number of players connected
    # TYPE exneuland_clients gauge
    exneuland_clients #{playercount}

    # HELP exneuland_rooms Numbers of players by room
    # TYPE exneuland_rooms gauge
    #{Enum.reduce(players, %{}, fn x, acc -> Map.update(acc, x, 1, fn c -> c + 1 end) end) |> Enum.reduce("", fn {k, v}, acc -> acc <> "exneuland_rooms{room=\"#{k}\"} #{v}\n" end)}
    """

    send_resp(
      conn,
      200,
      metrics_string
    )
  end

  match _ do
    IO.puts("unexpected request: #{conn.method} #{conn.request_path}")
    send_resp(conn, 404, "not found")
  end

  @impl Plug.ErrorHandler
  def handle_errors(conn, %{kind: _kind, reason: reason, stack: _stack}) do
    send_resp(conn, conn.status, Poison.encode!(%{error: reason}))
  end
end
