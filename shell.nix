{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = with pkgs; [
    elixir_1_12
    rebar3
    protobuf

    # keep this line if you use bash
    bashInteractive
  ];
}
