defmodule Exneuland.MixProject do
  use Mix.Project

  def project do
    [
      app: :exneuland,
      version: "0.1.0",
      elixir: "~> 1.12",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :crypto],
      mod: {Exneuland.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:plug_cowboy, "~> 2.0"},
      {:protox, "~> 1.4"},
      {:distillery, "~> 2.1"},
      {:poison, "~> 5.0"},
      {:uuid, "~> 1.1"},
      {:joken, "~> 2.0"},
      {:libcluster, "~> 3.3"},
      {:memento, "~> 0.3.2"},
      {:httpoison, "~> 1.8"}
    ]
  end
end
