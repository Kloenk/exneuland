import Config

config :exneuland,
  port: String.to_integer(System.get_env("EXNEU_PORT", "4000")),
  token_secret: System.get_env("EXNEU_SECRET", "secret"),
  hub_url: System.get_env("EXNEU_HUBURL", "https://hub-staging.cccv.de"),
  hub_token: System.get_env("EXNEU_HUBTOKEN", ""),
  event_slug: System.get_env("EXNEU_SLUG", "rc3_2021")

config :libcluster,
  topologies: [
    main:
      case System.get_env("EXNEU_K8S", nil) do
        nil ->
          [
            strategy: Elixir.Cluster.Strategy.LocalEpmd
          ]

        _ ->
          [
            strategy: Elixir.Cluster.Strategy.Kubernetes.DNS,
            config: [
              service: System.get_env("K8S_SERVICE", "exneuland"),
              application_name: "exneuland"
            ]
          ]
      end
  ]
