# Workadventure Frontend <-> Backend Communication

[TOC]

## Capture
- Using Chrom(e|ium), Devtools, network tab, WS connection
- Packets as Base64, decode with e.g. Protox

## Communication

### Session Initialization

1. WS opened at wss://$host/pusher/room
    - Query String Arguments:
        - characterLayer or characterLayers (multiple): contain name of chosen layers
        - left, top, right, bottom: edges of the viewport
        - x, y: x/y position
        - roomId: URL starting at \_/
        - name: chosen display name
        - token: some kind of token?
2. First Message in WS, from Backend:
    - ServerToClientMessage containing a RoomJoinedMessage:
        - currentUserId: server-chosen UID to refer to user. Unsure if room, namespace or server-unique
        - item: unsure, left blank
        - tag: unsure, left blank
3. Response from Client:
   - ClientToServerMessage containing a SilentMessage (silent = true)
   
### On Movement

1. Moving client:
   - Sends ClientToServerMessage containing a UserMovesMessage with updated position and viewport
2. Server:
   - Broadcasts a ServerToClientMessage containing a BatchMessage with 1 or multiple UserMovedMessage
   - drops viewport info
   - Batching currently at most 2 messages, always same user? Needs more investigation
   - Investigate if batching is effective for performance
   
### WebRTC (ad-hoc video conference)
1. Clients move into range
2. Server sends WebRtcStartMessage to both (all?) clients, designates one as initiator of the PtP connection
3. Client initates connection, sends WebRtcSignalToServerMessage containing webrtc foo
    - Server forwards all WebRtcSignalToServerMessages to clients (turning them into WebRtcSignalToClientMessage)
4. Server sends GroupUpdateMessage to create a new group and draw the circle
5. When a player exits range, Server terminates connection with WebRtcDisconnectMessages
6. Circle is destroyed with GroupDeleteMessage

